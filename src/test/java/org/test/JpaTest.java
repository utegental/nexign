package org.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.test.domain.Address;
import org.test.domain.Person;
import org.test.domain.Phone;
import org.test.repository.AddressRepository;
import org.test.repository.PersonRepository;
import org.test.repository.PhoneRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class JpaTest {
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PhoneRepository phoneRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Test
    public void test() {
        Person person = personRepository.save(Person.builder().name("asd").build());
        person.addPhone(phoneRepository.save(Phone.builder().person(person).value("zxc").build()));
        person.addAddress(addressRepository.save(Address.builder().person(person).value("vcx").build()));

        personRepository.save(person);
        Person retrieved = personRepository.getOne(person.getId());

        assertNotNull(retrieved.getPhones());
        assertEquals(1, retrieved.getPhones().size());

        assertNotNull(retrieved.getAddresses());
        assertEquals(1, retrieved.getAddresses().size());
    }
}
