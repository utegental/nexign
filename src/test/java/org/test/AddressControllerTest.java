package org.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.test.domain.Person;
import org.test.dto.AddressUI;
import org.test.rest.AddressController;
import org.test.service.AddressService;
import org.test.service.PersonService;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AddressController.class)
@AutoConfigureMockMvc
@AutoConfigureJsonTesters
public class AddressControllerTest {
    private static final Long PERSON_ID = 1L;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PersonService personService;

    @SuppressWarnings("unused")
    @MockBean
    private AddressService addressService;

    @Autowired
    private JacksonTester<AddressUI> phoneTester;

    @Before
    public void setup() {
        Person person = Person.builder()
            .id(PERSON_ID)
            .name("SomeName")
            .build();

        given(personService.getOne(any(Long.class)))
            .willReturn(person);
        given(personService.findAll())
            .willReturn(Collections.singletonList(person));
    }

    @Test
    public void testSaveDelete() throws Exception {
        AddressUI addressUI = new AddressUI()
            .id(1L)
            .value("8998");

        mvc.perform(
            post("/person/{personId}/address/", PERSON_ID)
                .content(phoneTester.write(addressUI).getJson())
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());

        mvc.perform(delete("/address/{addressId}", 1L))
            .andExpect(status().isOk());
    }
}
