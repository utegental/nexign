package org.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.test.domain.Person;
import org.test.dto.PersonUI;
import org.test.rest.PersonController;
import org.test.service.PersonService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonController.class)
@AutoConfigureMockMvc
@AutoConfigureJsonTesters
public class PersonControllerTest {

    private static final Long PERSON_ID = 1L;

    @Autowired
    private JacksonTester<PersonUI> personTester;

    @Autowired
    private JacksonTester<List<PersonUI>> personListTester;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PersonService personService;

    private Person person;

    private PersonUI personUI;

    @Before
    public void setup() {
        person = Person.builder()
            .id(PERSON_ID)
            .name("SomeName")
            .build();

        personUI = new PersonUI()
            .id(PERSON_ID)
            .name(person.getName());

        given(personService.findById(any(Long.class)))
            .willReturn(Optional.of(person));
        given(personService.findAll())
            .willReturn(Collections.singletonList(person));
    }

    @Test
    public void testGet() throws Exception {
        mvc.perform(get("/person/{id}", PERSON_ID).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().json(personTester.write(personUI).getJson()));

        mvc.perform(get("/person").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().json(personListTester.write(Collections.singletonList(personUI)).getJson()));
    }

    @Test
    public void testUpdateName() throws Exception {
        mvc.perform(put("/person/{id}", PERSON_ID).param("name", "SomeNewName"))
            .andExpect(status().isOk());
    }
}
