package org.test;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.test.domain.Address;
import org.test.domain.Person;
import org.test.domain.Phone;
import org.test.repository.PersonRepository;

import static java.util.Collections.singleton;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    @Profile("dev")
    public CommandLineRunner demo(PersonRepository personRepository) {
        return (args) -> personRepository.save(
            Person.builder()
                .name("SomeName")
                .phones(singleton(
                    Phone.builder().value("asd").build()
                ))
                .addresses(singleton(
                    Address.builder().value("zxc").build()
                ))
                .build()
        );
    }
}
