package org.test.dto;

public class AddressUI {
    private Long id;
    private String value;

    public Long getId() {
        return id;
    }

    public AddressUI id(Long id) {
        this.id = id;
        return this;
    }

    public String getValue() {
        return value;
    }

    public AddressUI value(String value) {
        this.value = value;
        return this;
    }
}
