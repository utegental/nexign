package org.test.dto;

public class PhoneUI {
    private Long id;
    private String value;

    public Long getId() {
        return id;
    }

    public PhoneUI id(Long id) {
        this.id = id;
        return this;
    }

    public String getValue() {
        return value;
    }

    public PhoneUI value(String value) {
        this.value = value;
        return this;
    }
}
