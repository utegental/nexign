package org.test.dto;

public class PersonUI {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public PersonUI id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public PersonUI name(String name) {
        this.name = name;
        return this;
    }
}
