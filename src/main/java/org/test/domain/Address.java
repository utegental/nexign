package org.test.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Address extends AbstractPersistable<Long> {
    private String value;

    @JoinColumn(nullable = false)
    @ManyToOne(optional = false)
    private Person person;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final Address mutable = new Address();

        public Address build() {
            return mutable;
        }

        public Builder id(Long id) {
            mutable.setId(id);
            return this;
        }

        public Builder value(String value) {
            mutable.value = value;
            return this;
        }

        public Builder person(Person person) {
            mutable.person = person;
            return this;
        }
    }
}
