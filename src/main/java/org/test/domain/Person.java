package org.test.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Person extends AbstractPersistable<Long> {

    private String name;

    @OneToMany(mappedBy = "person", cascade = CascadeType.PERSIST)
    private Set<Phone> phones = new HashSet<>();
    @OneToMany(mappedBy = "person", cascade = CascadeType.PERSIST)
    private Set<Address> addresses = new HashSet<>();

    public Person() {
    }

    public Set<Phone> getPhones() {
        return phones;
    }

    public void setPhones(Set<Phone> phones) {
        this.phones = phones;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public void addPhone(Phone phone) {
        phones.add(phone);
    }

    public void addAddress(Address address) {
        addresses.add(address);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final Person mutable = new Person();

        public Person build() {
            return mutable;
        }

        public Builder id(Long id) {
            mutable.setId(id);
            return this;
        }

        public Builder name(String name) {
            mutable.name = name;
            return this;
        }

        public Builder phones(Set<Phone> phones) {
            mutable.phones = phones;
            phones.forEach(a -> a.setPerson(mutable));
            return this;
        }

        public Builder addresses(Set<Address> addresses) {
            mutable.addresses = addresses;
            addresses.forEach(a -> a.setPerson(mutable));
            return this;
        }
    }
}
