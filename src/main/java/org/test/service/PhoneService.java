package org.test.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.test.domain.Phone;

@Service
public class PhoneService extends ServiceBase<Phone> {
    public PhoneService(JpaRepository<Phone, Long> repository) {
        super(repository);
    }

}
