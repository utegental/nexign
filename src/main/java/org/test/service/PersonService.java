package org.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.test.domain.Person;
import org.test.repository.PersonRepository;

@Service
public class PersonService extends ServiceBase<Person> {

    @Autowired
    public PersonService(PersonRepository personRepository) {
        super(personRepository);
    }

    public void updateName(Long personId, String name) {
        Person person = getRepository().getOne(personId);
        person.setName(name);
        getRepository().save(person);
    }
}
