package org.test.service;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Optional;

public abstract class ServiceBase<T> {

    private final JpaRepository<T, Long> repository;

    ServiceBase(JpaRepository<T, Long> repository) {
        this.repository = repository;
    }

    public T getOne(Long id) {
        return repository.getOne(id);
    }

    public Optional<T> findById(Long id) {
        return repository.findById(id);
    }

    public Collection<T> findAll() {
        return repository.findAll();
    }

    public void save(T item) {
        repository.save(item);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    JpaRepository<T, Long> getRepository() {
        return repository;
    }
}
