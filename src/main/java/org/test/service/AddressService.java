package org.test.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.test.domain.Address;

@Service
public class AddressService extends ServiceBase<Address> {
    public AddressService(JpaRepository<Address, Long> repository) {
        super(repository);
    }
}
