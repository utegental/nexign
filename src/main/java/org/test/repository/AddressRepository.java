package org.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.test.domain.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
}
