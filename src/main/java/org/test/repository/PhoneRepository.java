package org.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.test.domain.Phone;

@Repository
public interface PhoneRepository extends JpaRepository<Phone, Long> {
}
