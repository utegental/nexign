package org.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.test.domain.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

}
