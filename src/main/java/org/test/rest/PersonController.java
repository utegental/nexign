package org.test.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.test.domain.Person;
import org.test.dto.PersonUI;
import org.test.service.PersonService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("person")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("{id}")
    public ResponseEntity<PersonUI> get(@PathVariable("id") Long id) {
        return personService.findById(id)
            .map(this::map)
            .map(ResponseEntity::ok)
            .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping
    public List<PersonUI> get() {
        return personService.findAll()
            .stream()
            .map(this::map)
            .collect(Collectors.toList());
    }

    @PutMapping(value = "{id}")
    public void updateName(@PathVariable("id") Long personId, @RequestParam("name") String name) {
        personService.updateName(personId, name);
    }

    @PostMapping
    public void create(@Valid PersonUI person) {
        personService.save(map(person));
    }

    @DeleteMapping(value = "{id}")
    public void delete(@RequestParam("id") Long id) {
        personService.delete(id);
    }

    private PersonUI map(Person person) {
        return new PersonUI()
            .id(person.getId())
            .name(person.getName());
    }

    private Person map(PersonUI person) {
        return Person.builder()
            .id(person.getId())
            .name(person.getName())
            .build();
    }
}
