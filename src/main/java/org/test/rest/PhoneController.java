package org.test.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.test.domain.Phone;
import org.test.dto.PhoneUI;
import org.test.service.PersonService;
import org.test.service.PhoneService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
public class PhoneController {

    private final PhoneService phoneService;
    private final PersonService personService;

    @Autowired
    public PhoneController(PhoneService phoneService, PersonService personService) {
        this.phoneService = phoneService;
        this.personService = personService;
    }

    @PostMapping("person/{personId}/phone")
    public void save(@NotNull @PathVariable("personId") Long personId, @Valid PhoneUI phoneUI) {
        phoneService.save(map(phoneUI, personId));
    }

    @DeleteMapping("phone/{phoneId}")
    public void delete(@NotNull @PathVariable("phoneId") Long phoneId) {
        phoneService.delete(phoneId);
    }

    private Phone map(PhoneUI phone, Long personId) {
        return Phone.builder()
            .id(phone.getId())
            .value(phone.getValue())
            .person(personService.getOne(personId))
            .build();
    }
}
