package org.test.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.test.domain.Address;
import org.test.dto.AddressUI;
import org.test.service.AddressService;
import org.test.service.PersonService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
public class AddressController {
    private final AddressService addressService;
    private final PersonService personService;

    @Autowired
    public AddressController(AddressService addressService, PersonService personService) {
        this.addressService = addressService;
        this.personService = personService;
    }

    @PostMapping("person/{personId}/address")
    public void save(@NotNull @PathVariable("personId") Long personId, @Valid AddressUI addressUI) {
        addressService.save(map(addressUI, personId));
    }

    @DeleteMapping("address/{addressId}")
    public void delete(@NotNull @PathVariable("addressId") Long addressId) {
        addressService.delete(addressId);
    }

    private Address map(AddressUI address, Long personId) {
        return Address.builder()
            .id(address.getId())
            .value(address.getValue())
            .person(personService.getOne(personId))
            .build();
    }
}
